/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automationexamples;

import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author Nadir
 */
public class AutomationExamples {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.setProperty("webdriver.gecko.driver", "D:\\Testing\\Setup\\Browser Drivers\\geckodriver.exe");
        NavigateExample GoogleYahoo = new NavigateExample(new FirefoxDriver());
        String[] URLS = {"http://google.com","http://yahoo.com","http://facebook.com"};
        //GoogleYahoo.navigateUrls(URLS);
        GoogleYahoo.navigateUrls(URLS,NavigateExample.OPEN_IN_NEW_TAB);
    }
    
}
