/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package automationexamples;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sourceforge.htmlunit.corejs.javascript.JavaScriptException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author Nadir
 */
public class NavigateExample {
    private WebDriver driver;
    private long WAIT_TIME;
    public static final int OPEN_IN_NEW_TAB=1;
    NavigateExample(WebDriver driver){
        if(driver!=null)
        this.driver = driver;
        else System.out.println("Driver is NULL (Not initialized)");
        
        WAIT_TIME=0;
    }
    public void navigateUrls(String[] URLS)
    {
        for(String URL:URLS) showURL(URL);
    }
    
    public void waitFor(long time)
    {
        WAIT_TIME=time;
    }
    
    private void showURL(String URL)
    {
        driver.get(URL);
        try {
            Thread.sleep(WAIT_TIME);
        } catch (InterruptedException ex) {
            Logger.getLogger(NavigateExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void navigateUrls(String[] URLS, int OPTION) {
        for(int i=0;i<URLS.length;i++) 
        {
            if(i>0)showURL(URLS[i],OPTION);
            else showURL(URLS[i]);
        } 
    }

    private void showURL(String URL, int OPTION) {
        
        if(OPTION==OPEN_IN_NEW_TAB)
        {
                createTAB();
                switchToNewTAB();
        }
        
        showURL(URL);
        
        
    }
    
   

    private void createTAB() {
         try {
                Robot r = new Robot();
                r.keyPress(KeyEvent.VK_CONTROL);
                r.keyPress(KeyEvent.VK_T);
                r.keyRelease(KeyEvent.VK_CONTROL);
                r.keyRelease(KeyEvent.VK_T); 
                
            } catch (AWTException ex) {
                Logger.getLogger(NavigateExample.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    private void switchToNewTAB() {
        Set set=driver.getWindowHandles();
        
        for(Iterator<String> it=set.iterator();it.hasNext();) 
            if(it.hasNext()) driver.switchTo().window(it.next());
    }
}
