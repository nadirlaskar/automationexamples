# README #

You Need Java Environment set up and Selenium Library Package downloaded.

### What is this repository for? ###

* This repository contains examples on selenium web driver automation.
* v0.01
* https://nadirlaskar@bitbucket.org/nadirlaskar/automationexamples.git


### Contribution guidelines ###

* Fork the repository, add examples and submit pull request.
* Added Examples with specific class names to explain the example purpose.

### Who do I talk to? ###

* Submit your issue here.
* contact me at nadirlaskar@gmail.com